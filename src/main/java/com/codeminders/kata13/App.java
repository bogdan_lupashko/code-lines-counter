package com.codeminders.kata13;

import com.codeminders.kata13.service.impl.CatalogServiceImpl;
import com.codeminders.kata13.service.impl.JavaFileProcessor;

import java.nio.file.Paths;

public class App {

  public static void main(String[] args) {

    if (args.length != 0 && "-?".equals(args[0])) {
      System.out.println("Please provide file path or dir path as parameter to run line calc for java files.");
      System.exit(0);
    }
    var path = args.length == 0 ? "" : args[0];

    System.out.println("Started calc lines for" + Paths.get(path).toAbsolutePath().toString());

    new CatalogServiceImpl(new JavaFileProcessor())
        .catalogTreeProcessing(path)
        .forEach((k, v) -> System.out.println(k + ":" + v));

    System.out.println("Finished calc lines for" + Paths.get(path).toAbsolutePath().toString());
  }
}
