package com.codeminders.kata13.service;

public interface FileProcessor {
  int calcCodeLines(String content);
}
