package com.codeminders.kata13.service.data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Match {
  private int start;
  private String text;
}