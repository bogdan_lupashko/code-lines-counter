package com.codeminders.kata13.service.impl;

import com.codeminders.kata13.service.CatalogService;
import com.codeminders.kata13.service.FileProcessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CatalogServiceImplTest {
  private CatalogService catalogService;
  private FileProcessor fileProcessor;

  @Before
  public void setUp() {
    fileProcessor = mock(JavaFileProcessor.class);
    when(fileProcessor.calcCodeLines(anyString())).thenReturn(3);
    catalogService = new CatalogServiceImpl(fileProcessor);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void catalogTreeProcessingFileTest() {
    var path = "src/test/resources/AppMultiLineComment.java";
    var result = catalogService.catalogTreeProcessing(path);

    assertNotNull(result);
    assertEquals(1, result.size());
    assertEquals(Integer.valueOf(3), result.get(Paths.get(path)));

  }

  @Test
  public void catalogTreeProcessingDirTest1() {
    when(fileProcessor.calcCodeLines(anyString())).thenReturn(1);
    var path = "src/test/resources/";
    var result = catalogService.catalogTreeProcessing(path);

    assertNotNull(result);
    assertEquals(16, result.size());
    assertEquals(Integer.valueOf(12), result.get(Paths.get(path)));

  }

  @Test
  public void catalogTreeProcessingDirTest2() {
    when(fileProcessor.calcCodeLines(anyString())).thenReturn(1);
    var path = "src/test/resources";
    var result = catalogService.catalogTreeProcessing(path);

    assertNotNull(result);
    assertEquals(16, result.size());
    assertEquals(Integer.valueOf(12), result.get(Paths.get(path)));

  }

}
