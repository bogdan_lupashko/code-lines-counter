package com.codeminders.kata13.service.impl;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class JavaFileProcessorTest {

  private JavaFileProcessor processor;

  @Before
  public void setUp() {
    processor = new JavaFileProcessor();
  }

  @After
  public void tearDown() {
    processor = null;
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTestAllVariants() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppAllvariantInOneFile.java"))));
    assertEquals(48, result);
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTest1() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppMultiLineComment.java"))));
    assertEquals(16, result);
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTest2() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppMultiLineCommentInsideString.java"))));
    assertEquals(18, result);
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTest3() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppSingleLineComment.java"))));
    assertEquals(17, result);
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTest4() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppSingleLineCommentInsideString.java"))));
    assertEquals(18, result);
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTest5() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppSingleLineCommentWithMultiLineInside.java"))));
    assertEquals(17, result);
  }

  @SneakyThrows
  @Test
  public void calcCodeLinesTest6() {
    var result = processor.calcCodeLines(new String(Files.readAllBytes(Paths.get(
        "src/test/resources/AppSingleLineCommentWithQuotes.java"))));
    assertEquals(13, result);
  }

}