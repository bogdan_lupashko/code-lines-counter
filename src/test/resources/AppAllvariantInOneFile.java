package base;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppAllvariantInOneFile {
  static List<Match> commentMatches = new ArrayList<>();

  public  void main(String[] args) throws IOException {





    Pattern commentsPattern = Pattern.compile("(//.*?$)|(/\\*.*?\\*/)", Pattern.MULTILINE | Pattern.DOTALL);
    Pattern stringsPattern = Pattern.compile("(\".*?(?<!\\\\)\")");

    String text = new String(Files.readAllBytes(Paths.get("src/main/java/org/bl/kata13/AppAllvariantInOneFile.java")));

    Matcher commentsMatcher = commentsPattern.matcher(text);
    while (commentsMatcher.find()) {
      Match match = new Match();
      match.start = commentsMatcher.start();
      match.text = commentsMatcher.group();
      commentMatches.add(match);
    }

    List<Match> commentsToRemove = new ArrayList<>();

    Matcher stringsMatcher = stringsPattern.matcher(text);
    while (stringsMatcher.find()) {
      for (Match comment : commentMatches) {
        if (comment.start > stringsMatcher.start() && comment.start < stringsMatcher.end())
          commentsToRemove.add(comment);
      }
    }
    for (Match comment : commentsToRemove)
      commentMatches.remove(comment);

    for (Match comment : commentMatches)
      text = text
          .replace(comment.text, " ").trim()
          .replaceAll("(?m)^[ \t]*\r?\n", "");

    // we comment out some code
    System.out.println("We can use / inside a string of course");
    //we end the comment */
    var text = "text";

    System.out.println(text);
    System.out.println(text.split("\r\n|\r|\n").length);

  }

  //Single-line

  // "String? Nope"

  /*
   * "This  is not String either"
   */

  //Complex */
  ///*More complex*/

  /*Single line, but */

  String moreFun = " /* comment? doubt that */";

  String evenMoreFun = " // comment? doubt that ";

  // we co/*mment out some code
  //we end the comment */

  static class Match {
    int start;
    String
        text;
  }

}
//jgjhvjhvh
//kh