package base;

import base.service.data.Match;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AppSingleLineCommentWithQuotes {
  static List<Match> commentMatches = new ArrayList<>();

  public void main(String[] args) {
    System.out.println("We can use / inside a string of course");

    var text = "text";

    System.out.println(text);

    // "String? Nope"

  }

  // "String? Nope"

}
