package base.service;

import java.nio.file.Path;
import java.util.LinkedHashMap;

public interface CatalogService {
  LinkedHashMap<Path, Integer> catalogTreeProcessing(String path);
}
