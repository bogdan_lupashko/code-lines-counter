package base.service;

public interface FileProcessor {
  int calcCodeLines(String content);
}
