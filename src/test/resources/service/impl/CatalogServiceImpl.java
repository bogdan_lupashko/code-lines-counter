package base.service.impl;

import com.codeminders.kata13.service.CatalogService;
import com.codeminders.kata13.service.FileProcessor;
import lombok.SneakyThrows;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

public class CatalogServiceImpl implements CatalogService {

  private FileProcessor fileProcessor;

  public CatalogServiceImpl(FileProcessor fileProcessor) {
    this.fileProcessor = fileProcessor;
  }

  @Override
  public LinkedHashMap<Path, Integer> catalogTreeProcessing(String path) {

    var result = new LinkedHashMap<Path, Integer>();

    catalogTreeProcessing(Paths.get(path), result);

    return result;
  }

  private void catalogTreeProcessing(Path path, LinkedHashMap<Path, Integer> result) {
    try (var pathStream = Files.walk(path)) {

      pathStream.forEachOrdered(childPath -> {
        if (Files.isRegularFile(childPath)) {
          result.put(childPath, processFile(childPath));
        } else {
          if (childPath.equals(path)) {
            result.put(childPath, null);
          } else {
            catalogTreeProcessing(childPath, result);
          }
        }
      });

      result.putIfAbsent(path, result.entrySet().stream()
          .filter(e -> e.getKey().toAbsolutePath().getParent().equals(path.toAbsolutePath()))
          .mapToInt(Map.Entry::getValue)
          .sum());

    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  @SneakyThrows
  private Integer processFile(Path path) {
    return fileProcessor.calcCodeLines(new String(Files.readAllBytes(path)));
  }

}
