package base.service.impl;

import com.codeminders.kata13.service.FileProcessor;
import com.codeminders.kata13.service.data.Match;

import java.util.LinkedList;
import java.util.regex.Pattern;

public class JavaFileProcessor implements FileProcessor {
  private static final Pattern JAVA_COMMENT_PATTERN = Pattern.compile("(//.*?$)|(/\\*.*?\\*/)",
      Pattern.MULTILINE | Pattern.DOTALL);
  private static final Pattern JAVA_STRING_PATTERN = Pattern.compile("(\".*?(?<!\\\\)\")");

  @Override
  public int calcCodeLines(String content) {
    var commentsMatcher = JAVA_COMMENT_PATTERN.matcher(content);
    var commentMatches = new LinkedList<Match>();

    while (commentsMatcher.find()) {
      commentMatches.add(Match.builder()
          .start(commentsMatcher.start())
          .text(commentsMatcher.group())
          .build());
    }

    var stringsMatcher = JAVA_STRING_PATTERN.matcher(content);

    var commentsToRemove = new LinkedList<Match>();
    while (stringsMatcher.find()) {
      commentMatches.stream()
          .filter(comment -> comment.getStart() > stringsMatcher.start() && comment.getStart() < stringsMatcher.end())
          .forEach(commentsToRemove::add);
    }
    commentsToRemove.forEach(commentMatches::remove);

    for (var comment : commentMatches) {
      content = content
          .replace(comment.getText(), " ")
          .trim();
    }

    return content
        .replaceAll("(?m)^[ \t]*\r?\n", "")
        .split("\r\n|\r|\n").length;

  }
}
