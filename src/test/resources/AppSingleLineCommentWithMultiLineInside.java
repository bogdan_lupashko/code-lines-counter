package base;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppSingleLineCommentWithMultiLineInside {
  static List<Match> commentMatches = new ArrayList<>();

  public  void main(String[] args) throws IOException {
    System.out.println("We can use / inside a string of course");

    // we co/*mment out some code
    //we end the comment */



    var text = "text";

    System.out.println(text);




    //Complex */
    ///*More complex*/



  }

  // we co/*mment out some code
  var text = "text";
  //we end the comment */

  //Complex */
  ///*More complex*/


}
