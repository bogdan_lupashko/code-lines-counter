**Requirements:** 

- java 11
- maven 3 and above (for build only)

**Run**
```
java -jar app-1.0.jar  {path}
```

**Notice**
```
path could be realtive or absolute
also it could be dir or file reference
```

**Example**:

from cloned project dir  
```
java -jar app-1.0.jar  src/test/resources/AppMultiLineComment.java
```

**Test**
```
$ mvn clean test
```

**Build**
```
$ mvn clean compile assembly:single
```

**Contact**

bogdanlupashko@gmail.com
